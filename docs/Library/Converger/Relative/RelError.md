# RelError

Relative Error

This converger tests the following ratio,

$$
\left|\mathbf{\dfrac{E}{U}}\right|\leqslant{}tol.
$$

where $$\mathbf{E}$$ denotes the error vector set by other solvers, etc., and $$\mathbf{U}$$ denotes the total
displacement of the system.